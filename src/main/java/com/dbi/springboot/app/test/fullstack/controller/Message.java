package com.dbi.springboot.app.test.fullstack.controller;

import com.dbi.springboot.app.test.fullstack.model.dto.BaseResponseDTO;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/message")
@CrossOrigin(origins = { "http://localhost:4200" })


public class Message {

	@Value("${build.version}")
	private String version;
	
    @GetMapping("/welcome")
    public ResponseEntity<BaseResponseDTO> message() {
        BaseResponseDTO baseResponseDTO = new BaseResponseDTO();
        baseResponseDTO.setCode(201);
        baseResponseDTO.setMessage("Bienvenido Candidato 01");
        return new ResponseEntity<>(baseResponseDTO, HttpStatus.OK);
    }
    
    @GetMapping("/version")
    public ResponseEntity<BaseResponseDTO> version() {
        BaseResponseDTO baseResponseDTO = new BaseResponseDTO();
        baseResponseDTO.setCode(201);
        baseResponseDTO.setMessage("version: " + version);
        return new ResponseEntity<>(baseResponseDTO, HttpStatus.OK);
    }
}
