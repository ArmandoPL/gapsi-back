package com.dbi.springboot.app.test.fullstack.model.dto;

import java.io.Serializable;

public class ProovedorDTO implements Serializable {

	private static final long serialVersionUID = -3314608987638530719L;

	private Long id;
	private String name;
	private String socialReason;
	private String address;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSocialReason() {
		return socialReason;
	}

	public void setSocialReason(String socialReason) {
		this.socialReason = socialReason;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "{\"id\":" + id + ", \"name\":\"" + name + "\", \"socialReason\":\"" + socialReason + "\", \"address\":\"" + address
				+ "\"}";
	}

}
