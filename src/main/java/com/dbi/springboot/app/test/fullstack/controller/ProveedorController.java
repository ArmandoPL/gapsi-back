package com.dbi.springboot.app.test.fullstack.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.dbi.springboot.app.test.fullstack.model.dto.BaseResponseDTO;
import com.dbi.springboot.app.test.fullstack.model.dto.ProovedorDTO;
import com.dbi.springboot.app.test.fullstack.service.inter.ProovedorServiceInt;
import com.dbi.springboot.app.test.fullstack.util.ServiceException;
import com.dbi.springboot.app.test.fullstack.util.constant.Constant;

@RestController
@RequestMapping("/api/provider")
@CrossOrigin(origins = { "http://localhost:4200" })


public class ProveedorController {

	@Autowired
	private ProovedorServiceInt providerService;

	/**
	 * Controlador para crear un proovedor
	 * 
	 * @return Objeto de confirmacion de proovedor creado
	 */
	@PostMapping("")
	public ResponseEntity<BaseResponseDTO> create(@RequestBody ProovedorDTO provider) {
		BaseResponseDTO baseResponseDTO = new BaseResponseDTO();
		try {
			ProovedorDTO response = providerService.create(provider);

			baseResponseDTO.setCode(Constant.SUCCESS);
			baseResponseDTO.setMessage("Proovedores Registrado");
			baseResponseDTO.setResponseData(response);
		} catch (ServiceException e) {
			baseResponseDTO.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			baseResponseDTO.setMessage(e.getMessage());
			return new ResponseEntity<>(baseResponseDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(baseResponseDTO, HttpStatus.OK);

	}

	/**
	 * Controlador para listar a los proovedores
	 * 
	 * @return Objeto con lista de proovedores
	 */
	@GetMapping("")
	public ResponseEntity<BaseResponseDTO> list() {
		BaseResponseDTO baseResponseDTO = new BaseResponseDTO();
		try {
			List<ProovedorDTO> response = providerService.list();
			baseResponseDTO.setCode(Constant.SUCCESS);
			baseResponseDTO.setMessage("Proovedores Obtenidos");
			baseResponseDTO.setResponseData(response);
		} catch (ServiceException e) {
			baseResponseDTO.setCode(e.getIdError());
			baseResponseDTO.setMessage(e.getMessage());
			return new ResponseEntity<>(baseResponseDTO, HttpStatus.valueOf(e.getIdError()));
		}

		return new ResponseEntity<>(baseResponseDTO, HttpStatus.OK);

	}

	/**
	 * Controlador para eliminar un proovedor
	 * 
	 * @return Confirmacion de proovedor eliminado
	 */
	@DeleteMapping("")
	public ResponseEntity<BaseResponseDTO> delete(@RequestParam(name = "idProvider") Long id) {
		BaseResponseDTO baseResponseDTO = new BaseResponseDTO();
		try {
			providerService.delete(id);
			baseResponseDTO.setCode(Constant.SUCCESS);
			baseResponseDTO.setMessage("Proovedor eliminado");
		} catch (ServiceException e) {
			baseResponseDTO.setCode(e.getIdError());
			baseResponseDTO.setMessage(e.getMessage());
			return new ResponseEntity<>(baseResponseDTO, HttpStatus.valueOf(e.getIdError()));
		}

		return new ResponseEntity<>(baseResponseDTO, HttpStatus.OK);
	}
}
