package com.dbi.springboot.app.test.fullstack.service.inter;

import java.util.List;

import com.dbi.springboot.app.test.fullstack.model.dto.ProovedorDTO;
import com.dbi.springboot.app.test.fullstack.util.ServiceException;

public interface ProovedorServiceInt {

	/**
	 * Servicio para la creacion de un proovedor
	 * @param provider Objeto de proovedor
	 * @return objeto de proovedor creado
	 * @throws ServiceException 
	 */
	ProovedorDTO create(ProovedorDTO provider) throws ServiceException;

	/**
	 * Servicio para obtener el listado de proovedores
	 * @return Lista de proovedores
	 * @throws ServiceException 
	 */
	List<ProovedorDTO> list() throws ServiceException;

	/**
	 * Servicio para eliminar un proovedor
	 * @param id ID del proovedor
	 * @throws ServiceException 
	 */
	void delete(Long id) throws ServiceException;

}
