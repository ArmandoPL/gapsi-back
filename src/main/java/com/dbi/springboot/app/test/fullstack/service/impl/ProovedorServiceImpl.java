package com.dbi.springboot.app.test.fullstack.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.dbi.springboot.app.test.fullstack.model.dto.ProovedorDTO;
import com.dbi.springboot.app.test.fullstack.service.inter.ProovedorServiceInt;
import com.dbi.springboot.app.test.fullstack.util.ServiceException;
import com.dbi.springboot.app.test.fullstack.util.WriteFile;

@Service
public class ProovedorServiceImpl implements ProovedorServiceInt {

	@Override
	public ProovedorDTO create(ProovedorDTO provider) throws ServiceException {
		try {
			List<ProovedorDTO> providers = new ArrayList<>(WriteFile.read());
			ProovedorDTO existProvider = providers.stream().filter(p -> p.getName().equals(provider.getName())).findAny().orElse(null);
			if (existProvider == null) {
				provider.setId(providers.size() == 0 ? 1 : providers.get(providers.size() - 1).getId() + 1);
				providers.add(provider);
				System.out.println("Se registra proovedor");
				WriteFile.write(providers);
			} else {
				throw new ServiceException("El proovedor ya existe");
			}
		} catch (ServiceException e) {
			throw new ServiceException(e.getMessage());
		}

		return provider;
	}


	@Override
	public List<ProovedorDTO> list() throws ServiceException {
		List<ProovedorDTO> providers = new ArrayList<>();
		try {
			providers = WriteFile.read();
			if (providers.isEmpty() || providers == null) {
				throw new ServiceException(404, "No se ha encontrado ningun registro");
			}
		} catch (ServiceException e) {
			throw new ServiceException(e.getIdError(), e.getMessage());
		}
		return providers;
	}
	

	@Override
	public void delete(Long id) throws ServiceException {
		try {
			List<ProovedorDTO> providers = new ArrayList<>(WriteFile.read());
			ProovedorDTO providerDelete = providers.stream().filter(p -> p.getId() == id).findAny().orElse(null);
			providers.remove(providerDelete);
			if (providerDelete == null) {
				throw new ServiceException(404, "El proovedor no existe");
			} else {
				System.out.println("Se elimina proovedor");
				WriteFile.write(providers);				
			}
		} catch (ServiceException e) {
			throw new ServiceException(e.getIdError(), e.getMessage());
		}

	}

}
