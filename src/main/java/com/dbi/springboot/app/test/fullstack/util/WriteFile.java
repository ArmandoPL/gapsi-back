package com.dbi.springboot.app.test.fullstack.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import com.dbi.springboot.app.test.fullstack.model.dto.ProovedorDTO;
import com.fasterxml.jackson.databind.ObjectMapper;

public class WriteFile {
	
	/**
	 * Metodo usado para poder escribir en el archivo de base de datos
	 * 
	 * @param providers Objeto de proovedores
	 * @throws ServiceException
	 */
	public static void write(List<ProovedorDTO> providers) throws ServiceException {
		Path path = Paths.get("src/main/resources/bd.json");
		try (BufferedWriter bw = Files.newBufferedWriter(path, StandardCharsets.UTF_8)) {
			ObjectMapper mapper = new ObjectMapper();
			String jsonString = mapper.writeValueAsString(providers);
			bw.write(jsonString);
			System.out.println("Successfully written data to the file");
		} catch (IOException e) {
			throw new ServiceException(404, "No se ha podido realizar el registro");
		}
	}

	/**
	 * Metodo usado para leer el archivo de base de datpps
	 * @return Objeto con la lista de proovedores
	 * @throws ServiceException Exception Generica
	 */
	public static List<ProovedorDTO> read() throws ServiceException {
		List<ProovedorDTO> providers = new ArrayList<>();
		try {
			String cadena = "";
			Scanner s = new Scanner(new BufferedReader(new FileReader("src/main/resources/bd.json")));
			while (s.hasNext()) {
				cadena = cadena + s.nextLine();
			}
			if (cadena != null && !cadena.isEmpty()) {
				ObjectMapper mapper = new ObjectMapper();
				ProovedorDTO[] providersArray = mapper.readValue(cadena, ProovedorDTO[].class);
				providers = Arrays.asList(providersArray);				
			}
		} catch (Exception e) {
			throw new ServiceException(404, "No se ha encontrado ningun registro");
		}
		return providers;
	}
}
