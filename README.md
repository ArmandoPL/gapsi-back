# Backend position test

Examen técnico para posición de fullstack developer


## Important


- Dentro del proyecto se incluye la documentacion del mismo
- La base de datos es un archivo json donde se escribe y lee informacion
```bash
  file: src/main/resources/bd.json
```
- La coleccion de postman viene dentro de la carpeta de resources
```bash
  file: src/main/resources/postman/gapsi-test.postman_collection.json
```
- Para levantar el proyecto es necesario tener instalado lo siguiente:
```bash
  * jdk 8
  * maven
```

## Run Locally

Clonar el proyecto

```bash
  git clone https://gitlab.com/ArmandoPL/gapsi-back.git

```

Ir al directorio del proyecto

```bash
  cd gapsi-back
```

Instalar dependencias

```bash
  mvn eclipse:eclipse
  mvn eclipse:clean
```

Iniciar el servidor

```bash
  mvn spring-boot:run
```

## Features

Tests
- Crear proovedor
```http
  POST  /api/provider/
```
- Obtener lista de proovedores
```http
  GET   /api/provider/
```
- Eliminar proovedor
```http
  DELETE  /api/provider/
```

## Feedback

Si tienes alguna retroalimentacion, porfavor contactame a mi correo armandoperalta648@gmail.com


## Authors

- [@ArmandoPL](https://gitlab.com/ArmandoPL)


##  Links
[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/armando-peralta-2138a81a0)

